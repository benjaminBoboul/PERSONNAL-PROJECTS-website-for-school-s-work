// Made By Benjamin
// Jquery plugin for this website



// Détecte quand les fichiers local que la page recherche sont bon
  $(document).ready(function() {
      setTimeout(function(){
        // ajoute la classe loaded au body qui nous sert d'interrupteur pour les effets
          $('body').addClass('loaded');
          // un petit log dans la console pour s'assurer du bon chargement du site (aussi utile qu'une licorne en Hollande, donc inutile)
          console.log("                        _   _       _a_a")
          console.log("            _   _     _{.`=`.}_    {/ ``|_")
          console.log("      _    {.`'`.}   {.'  _  '.}  {|  ._oo)")
          console.log("     { |  {/ .-. |} {/  .' '.  |} {/  |")
          console.log("~^~^~`~^~`~^~`~^~`~^~^~`^~^~`^~^~^~^~^~^~`^~~`")
          console.log("           website loaded, Welcome!           ");
          // petit timeout pour exécuter le code
      }, 1000);
  });

// smooth scroll
$(function() {
  // n'affiche pas les id dans les liens
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        // on anime le body jusqu'as la cible
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});


(function($, undefined){
  "use strict";

  var $navbar = $("#CHECK"),
      y_pos = $navbar.offset().top,
      height = $navbar.height();

  $(document).scroll(function(){    
    var scrollTop = $(this).scrollTop();

    if (scrollTop > y_pos + height){
      $('#navbar ul').addClass("navbar-fixed").animate({ top: "0px" }, 0);
      $('#ScrollTop').addClass("ScrollOn");
      $('.waypoints').addClass("wayctive");
      } else if (scrollTop <= y_pos){        
      $('#navbar ul').removeClass("navbar-fixed").clearQueue().animate({ top: "-250px" }, 0);
      $('#ScrollTop').removeClass("ScrollOn");
      $('.waypoints').removeClass("wayctive")
    }
  });
  
})(jQuery, undefined);


// on détecte le navigateur utilisé
var userAg = navigator.userAgent;
// si c'est Firefox
if (userAg.indexOf("Firefox")!=-1) {
  // on ajoute mozilla au body et ensuite le css s'occupe des soucis svg de ff
    $('body').addClass("mozilla");
  } else {

  }
